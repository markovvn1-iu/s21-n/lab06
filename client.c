#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include "sockets.h"

void print_help(char* prog)
{
	printf("Using: %s HOST_NAME PORT\n", prog);
}

int main(int argc, char** argv)
{
	if (argc != 3)
	{
		print_help(argv[0]);
		return -1;
	}

	char* host = argv[1];
	int port = atoi(argv[2]);
	printf("[ INFO ] Start server on port %d\n", port);


	printf("[ DEBUG ] Creating socket\n");
	SOCKET server_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (server_socket == INVALID_SOCKET)
	{
		printf("Error while create socket\n");
		return -2;
	}

	SOCKADDR_IN server_data;
	bzero((char*)&server_data, sizeof(server_data));
	server_data.sin_family = AF_INET;
	server_data.sin_port = htons(port);

	// Connect socket to port
	if (inet_pton(AF_INET, host, &server_data.sin_addr) < 0)
	{
		printf("[ ERROR ] Error while converting host address (%s) to struct in_addr\n", host);
		return -3;
	}

	// Setup listen mode
	printf("[ DEBUG ] Connecting to server %s:%d\n", host, port);
	if (connect(server_socket, (SOCKADDR*)&server_data, sizeof(server_data)) < 0)
	{
		printf("[ ERROR ] Error while connecting to server\n");
		return -4;
	}

	printf("[ DEBUG ] Connected to server\n");

	char msg[1024];

	while (1) {
		printf("Type message you want to send: ");

		scanf("%s", msg);
		unsigned int len = strlen(msg);
		send(server_socket, &len, 4, 0);
		send(server_socket, msg, len, 0);
	}

	close(server_socket);

	return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

#include "sockets.h"

void print_help(char* prog)
{
	printf("Using: %s PORT\n", prog);
}

int process_client(SOCKET client_socket)
{
	while (1)
	{
		unsigned int len = 0;
		recv(client_socket, &len, 4, 0);
		printf("lenght: %d\n", len);
		char* msg = malloc(len + 1);
		recv(client_socket, msg, len, 0);
		msg[len] = 0;

		printf("Resived: %s\n", msg);
		free(msg);
	}

	return 0;
}

int main(int argc, char** argv)
{
	if (argc != 2)
	{
		print_help(argv[0]);
		return -1;
	}

	int port = atoi(argv[1]);
	printf("[ INFO ] Start server on port %d\n", port);


	printf("[ DEBUG ] Creating socket\n");
	SOCKET server_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (server_socket == INVALID_SOCKET)
	{
		printf("Error while create socket\n");
		return -2;
	}

	// Allow to reuse same port
	printf("[ DEBUG ] Allowing to reuse port\n");
	int yes = 1;
	setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, (const char*)&yes, sizeof(int));

	SOCKADDR_IN server_data;
	bzero((char*)&server_data, sizeof(server_data));
	server_data.sin_family = AF_INET;
	server_data.sin_addr.s_addr = INADDR_ANY;
	server_data.sin_port = htons(port);

	// Connect socket to port
	printf("[ DEBUG ] Binding socket to port %d\n", port);
	if (bind(server_socket, (SOCKADDR*)&server_data, sizeof(server_data)) < 0)
	{
		printf("Error while binding socket");
		return -3;
	}

	// Setup listen mode
	printf("[ DEBUG ] Setuping listen mode\n");
	if (listen(server_socket, 5) < 0)
	{
		printf("[ ERROR ] Error while setup listen mode\n");
		return -4;
	}

	// Accept connection
	while (1) {
		printf("[ DEBUG ] Waiting for connection\n");
		SOCKET client_socket = accept(server_socket, NULL, NULL);
		if (client_socket == INVALID_SOCKET)
		{
			printf("[ DEBUG ] Accept invalid socket\n");
			continue;
		}
		printf("[ DEBUG ] Accept client\n");

		process_client(client_socket);

		close(client_socket);
	}
	close(server_socket);

	return 0;
}
